import 'dart:math';
import 'dart:ui';

import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'QuizBrain.dart';
import 'home.dart';

void main() => runApp(HomePageWidget());

QuizBrain _quizBrain = QuizBrain();

class Quizzler extends StatelessWidget {
  final int amountOfQuestions;
  const Quizzler(this.amountOfQuestions, {Key key}) : assert(amountOfQuestions > 0 && amountOfQuestions < 14), super(key: key);
  @override
  Widget build(BuildContext context) {
    print(amountOfQuestions);
    return
      // MaterialApp(
      // home:
    Scaffold(
        backgroundColor: Colors.grey.shade900,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            child: QuizPage(amountOfQuestions),
          ),
        ),
      // ),
    );
  }
}


class QuizPage extends StatefulWidget {
  final int amountOfQuestions;
  QuizPage(this.amountOfQuestions);

  @override
  _QuizPageState createState() => _QuizPageState(amountOfQuestions);
}

class _QuizPageState extends State<QuizPage> {

  AudioCache audioCache;
  static const correctAudioPath = "correct/correct";
  static const correctArrSize = 8;
  static const wrongAudioPath = "wrong/wrong";
  static const wrongArrSize = 7;

  Alert gameOverAlert;
  int correctQuestionsAnsweredCounter= 0;

  List<Widget> scoreKeeper = [
    Padding(
      padding: const EdgeInsets.fromLTRB(2,6,6,6),
      child: Text('Score:',
        style: TextStyle(
          fontSize: 14,
          color: Colors.white
        ),
      ),
    ),
  ];

  int amountOfQuestions;

  _QuizPageState(this.amountOfQuestions);


  @override
  void initState() {
    audioCache = new AudioCache();
    _quizBrain.setQuestionsCounter(amountOfQuestions);
    _quizBrain.restartGame();
    _quizBrain.rumbleQuestions();
    super.initState();
  }

  void checkAnswer(bool userPickedAnswer) {
    bool correctAnswer = _quizBrain.getQuestionAnswer();
    bool isUserCorrect = userPickedAnswer == correctAnswer;

    setState(() {
      bool result = _quizBrain.nextQuestion();
      if(result) {
        scoreKeeper.add(getIcon(isUserCorrect));

        if(isUserCorrect) {
          correctQuestionsAnsweredCounter++;
          print('got it right');
          playCorrectSound();
        } else {
          print('got it wrong');
          playWrongSound();
        }
      } else {
        showDialog("Game Over", "You finished with " + correctQuestionsAnsweredCounter.toString() + " correct questions", onPositiveCallback, onNegativeCallback);
      }
    });
  }

  void onPositiveCallback() {
    print("onPositiveCallback");
    // Navigator.pop(context);
    scoreKeeper.removeRange(1, scoreKeeper.length);
    setState(() {
      _quizBrain.restartGame();
      correctQuestionsAnsweredCounter = 0;
      _quizBrain.rumbleQuestions();
    });
    // _quizBrain.nextQuestion();// = true;
    // gameOverAlert.dismiss();
  }

  void onNegativeCallback() {
    print("onNegativeCallback");
    Navigator.pop(context);
  }

  void showDialog(String title, String msg, Function onPositiveCallback, Function onNegativeCallback) {
   gameOverAlert = Alert(
      context: context,
      type: AlertType.warning,
      title: title,
      desc: msg,
      buttons: [
        DialogButton(
          child: Text(
            "Play Again",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            onPositiveCallback();
            gameOverAlert.dismiss();
          },
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
        DialogButton(
          child: Text(
            "Home",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            onNegativeCallback();
          gameOverAlert.dismiss();
        },
          gradient: LinearGradient(colors: [
            Color.fromRGBO(116, 116, 191, 1.0),
            Color.fromRGBO(52, 138, 199, 1.0)
          ]),
        )
      ],
    );
   gameOverAlert.show();
  }

  Icon getIcon(bool isTrue) {
    if(isTrue) {
      return Icon(
        Icons.check,
        color: Colors.green,
      );
    } else {
      return Icon(
        Icons.close,
        color: Colors.red,
      );
    }
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Text(
            _quizBrain.getQuestionTitleText(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 22.0,
              color: Colors.white,
            ),
          ),
        ),
        Row(
          children: scoreKeeper,
        ),
        Expanded(
          flex: 5,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                _quizBrain.getQuestionText(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(15.0),
            // child: Opacity(
            //   opacity: quizBrain.getButtonsVisibility(),
              child: FlatButton(
                textColor: Colors.white,
                color: Colors.green,
                child: Text(
                  _quizBrain.getNextButtonText(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                  ),
                ),
                onPressed: () {
                  //The user picked true.
                  checkAnswer(true);
                },
              ),
            // ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: Opacity(
              opacity: _quizBrain.getButtonsVisibility(),
              child: FlatButton(
                color: Colors.red,
                child: Text(
                  'False',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  //The user picked false.
                  checkAnswer(false);
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  void playCorrectSound() {
    setState(() {
      int nextSoundIndex = Random.secure().nextInt(correctArrSize);
      audioCache.play(correctAudioPath + nextSoundIndex.toString() + ".wav");
    });

  }

  void playWrongSound() {
    setState(() {
      int nextSoundIndex = Random.secure().nextInt(wrongArrSize);
      audioCache.play(wrongAudioPath + nextSoundIndex.toString() + ".wav");
    });
  }
}