import 'dart:math';

import 'Question.dart';

class QuizBrain {

  int _questionNumber = 0;
  bool quizEnded = false;

  int _maxQuestionsNumber = 7-1;

  List<Question> _questionBank = [
    Question('Some cats are actually allergic to humans', true),
    Question('You can lead a cow down stairs but not up stairs', false),
    Question('Approximately one quarter of human bones are in the feet', true),
    Question('A slug\'s blood is green', true),
    Question('Buzz Aldrin\'s mother\'s maiden name was \"Moon\"', true),
    Question('It is illegal to pee in the Ocean in Portugal', true),
    Question(
        'No piece of square dry paper can be folded in half more than 7 times.',
        false),
    Question(
        'In London, UK, if you happen to die in the House of Parliament, you are technically entitled to a state funeral, because the building is considered too sacred a place.',
        true),
    Question(
        'The loudest sound produced by any animal is 188 decibels. That animal is the African Elephant',
        false),
    Question(
        'The total surface area of two human lungs is approximately 70 square metres',
        true),
    Question('Google was originally called \"Backrub\"', true),
    Question(
        'Chocolate affects a dog\'s heart and nervous system; a few ounces are enough to kill a small dog',
        true),
    Question(
        'In West Virginia, USA, if you accidentally hit an animal with your car, you are free to take it home to eat',
        true),
    Question('Friends star Lisa Kudrow was originally cast in the sitcom Frasier', true),
    Question('If you’re born between May 1st and 20th, then you’re a Gemini', false),
    Question('Emma Roberts is the daughter of Julia Roberts', false),
    Question('There are over 2,500 stars on the Hollywood Walk of Fame', true),
    Question('Fruit flies were the first living creatures sent into space', true),
    Question('Cyclones spin in a clockwise direction in the southern hemisphere', true),
    Question('Goldfish only have a memory of three seconds', false),
    Question('The capital of Libya is Benghazi', false),
    Question('Dolly Parton is the godmother of Miley Cyrus', true),
    Question('Roger Federer has won the most Wimbledon titles of any player', false),
    Question('An octopus has five hearts', false),
    Question('Brazil is the only country in the Americas to have the official language of Portuguese', true),
    Question('The Channel Tunnel is the longest rail tunnel in the world', false),
    Question('Darth Vader famously says the line “Luke, I am your father” in The Empire Strikes Back', false),
    Question('Olivia Newton-John represented the UK in the Eurovision Song Contest in 1974, the year ABBA won with “Waterloo”', true),
    Question('Stephen Hawking declined a knighthood from the Queen', true),
    Question('The highest mountain in England is Ben Nevis', false),
    Question('Nicolas Cage and Michael Jackson both married the same woman', true),
    Question('Japan and Russia did not sign a peace treaty after World War Two so are technically still at war', true),
    Question('The mathematical name for the shape of a Pringle is hyperbolic paraboloid', true),
  ];

  void rumbleQuestions() {
    // _maxQuestions
    _questionBank.shuffle(Random());
  }

  bool nextQuestion() {
    if(quizEnded) {
      //todo do something
    } else {
      if(_questionNumber < _questionBank.length - 1 && _questionNumber < _maxQuestionsNumber) {
        _questionNumber++;
        return true;
      } else if(_questionNumber == _questionBank.length - 1 || _questionNumber == _maxQuestionsNumber) {
        quizEnded = true;
        return true;
      }
    }

    return false;
  }

  void restartGame() {
    quizEnded = false;
    _questionNumber = 0;
  }

  int getQuestionNumber() {
    // todo if use random then use _maxQuestions to calculate
    // _maxQuestions
    return _questionNumber;
  }

  String getQuestionText() {
    if(quizEnded) {
      return "Round Over";
    } else {
      return _questionBank[_questionNumber].questionText;
    }
  }

  String getQuestionTitleText() {
    if(quizEnded) {
      return "";
    } else {
      return "Question #" + (_questionNumber + 1).toString();
    }
  }


  bool getQuestionAnswer() {
    return _questionBank[_questionNumber].questionAnswer;
  }

  double getButtonsVisibility() {
    if(quizEnded) {
      return 0;
    } else {
      return 1;
    }
  }

  String getNextButtonText() {
    if(quizEnded) {
      return "Next";
    } else {
      return "True";
    }
  }

  void setQuestionsCounter(int amountOfQuestions) {
    _maxQuestionsNumber = amountOfQuestions -1;
  }


}