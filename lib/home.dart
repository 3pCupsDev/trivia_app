import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

import 'main.dart';

class HomePageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.grey.shade900,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: HomePage(),
          ),
        ),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  // const HomePage({Key key}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  // int sliderValue = 7;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 42, 0, 0),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              '3ViA',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 42,
                  fontWeight: FontWeight.bold),
            ),
            OutlineButton(
              highlightColor: Colors.lightBlue,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  'Play!',
                  style: TextStyle(
                    fontSize: 22,
                  ),
                ),
              ),
              textColor: Colors.white,
              onPressed: () {
                _displayDifficultyDialog(context);
              },
            ),
            Column(
              children: <Widget>[
                DialogButton(
                  child: Text(
                    "More Apps & Games",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () {
                    openStoreLink();
                  },
                  gradient: LinearGradient(colors: [
                    Color.fromRGBO(116, 116, 191, 1.0),
                    Color.fromRGBO(255, 99, 52, 1.0)
                  ]),
                ),
                DialogButton(
                  child: Text(
                    "3P Cups",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () {
                    openWebsite();
                  },
                  gradient: LinearGradient(colors: [
                    Color.fromRGBO(116, 116, 191, 1.0),
                    Color.fromRGBO(52, 138, 199, 1.0)
                  ]),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void openWebsite() {
    print("openWebsite");
    openUrl("https://3p-cups.com/");
  }

  void openStoreLink() {
    print("openStoreLink");
    openUrl("https://play.google.com/store/apps/dev?id=8456795065374888880");
  }

  Future<void> openUrl(String url) async {
    print("openUrl $url");
    if (await canLaunch(url)) await launch(url);
  }

  Future<void> _displayDifficultyDialog(BuildContext context) async {

    int sliderValue = 7;

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Choose Difficulty'),
          content: Text('Select amount of Questions'),
          actions: <Widget>[
        Center(
          child: StatefulBuilder(
          builder: (context, state) => Slider(
            value: sliderValue.ceilToDouble(),
            min: 3,
            max: 13,
            divisions: 100,
            activeColor: Colors.green,
            inactiveColor: Colors.grey,
            label: '${sliderValue.round()}',
            semanticFormatterCallback: (double newValue) {
              return '${newValue.round()}';
            },
            onChanged: (val) {
              state(() {
                sliderValue = val.ceil();
              });
            },
          ),
          ),
        ),
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text('START'),
              onPressed: () {
                print(sliderValue);
                Navigator.pop(context); // CLOSE DIALOG

                int amountOfQuestions = sliderValue;
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Quizzler(amountOfQuestions)));
              },
            ),
          ],
        );
      },
    );
  }
}
